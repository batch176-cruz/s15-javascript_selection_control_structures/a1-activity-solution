console.log("Hello World");

let firstNum = parseInt(prompt("Provide a number"));
let secondNum = parseInt(prompt("Provide a number"));

let total = firstNum + secondNum;

if (total < 10) {
	console.warn(`The sum of the two numbers are: ${total}`);
} else if (total < 20) {
	alert(`The difference of the two numbers are: ${secondNum - firstNum}`);
} else if (total < 30) {
	alert(`The product of the two numbers are: ${firstNum * secondNum}`);
} else {
	alert(`The quotient of the two numbers are: ${firstNum / secondNum}`);
}

let name = prompt("What is your name?");
let age = prompt("What is your age?");

console.log(name);
console.log(age);

if (name.length == 0 || age.length == 0) {
	alert("Are you a time traveler?");
} else {
	alert(`Hello ${name}. Your age is ${age}`);
}

age = parseInt(age);

function isLegalAge(age) {
	if (age > 17) {
		alert("You are of legal age.");
	} else {
		alert("You are not allowed here.");
	}
}

isLegalAge(age);

switch (true) {
	case (age == 18):
		alert("You are now allowed to party.");
		break;
	case (age == 21):
		alert("You are now part of the adult society.");
		break;
	case (age == 65):
		alert("We thank you for your contribution to society.");
		break;
	default:
		alert("Are you sure you're not an alien?");
		break;
}

try {
	isLegalag(age);
} catch (error) {
	console.warn(error.message);
} finally {
	alert("use the function isLegalAge");
}1